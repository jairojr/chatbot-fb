import mysql from 'mysql';
import config from '../config';
import { promisify } from 'util';

const settingsDB = {
    user: config.dbUser,
    password: config.dbPassword,
    host: config.dbServer,
    database: config.dbDatabase,
    charset: 'utf8mb4_spanish_ci'
}

const pool = mysql.createPool(settingsDB);
pool.getConnection( (error, connection) => {
    if (error){
        console.error(error);
    }
    if (connection) connection.release();
    console.log('DB is Connected');
    return;
} );

pool.query = promisify(pool.query);

export {pool};
