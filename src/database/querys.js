export const queris ={
    getAllProducts: 'SELECT * FROM Products',
    addNewProduct: 'INSERT INTO Products (name, description, quantity) VALUES (@name, @description, @quantity)',
    getProductById: 'SELECT * FROM Products Where Id = @id',
    deleteProduct: 'DELETE FROM [webstore].[dbo].[Products] Where Id = @id',
    getTotalProducts: 'SELECT COUNT(*) FROM Products',
    updateProductById: 'UPDATE Products SET Name = @name, Description = @description, Quantity = @quantity Where Id = @id',
    addNewUser: 'INSERT INTO users (senderId, recipientId) VALUES (@senderId, @recipientId)'
}
