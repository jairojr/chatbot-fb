import { Router } from 'express';
import {
    deleteKeywordMessage,
    deleteTImageMessage,
    homePage,
    listKeywordMessage,
    listTImageMessage,
    saveKeywordMessage,
    saveTImageMessage,
    deleteTVideoMessage,
    saveTVideoMessage,
    listTVideoMessage,
    updateTVideoMessage,
    deleteTButtonMessage,
    saveTButtonMessage,
    listTButtonMessage,
    updateTButtonMessage,
    deleteTCarouselMessage,
    saveTCarouselMessage,
    listTCarouselMessage,
    updateTCarouselMessage,
    deleteTConexionMessage,
    saveTConexionMessage,
    listTConexionMessage,
    updateTConexionMessage,
    sendMessageCustom,
    tokenVerify,
    updateKeywordMessage,
    updateTImageMessage,
    validateEvent,
    totalClients, listBTNMenu, updateBTNMenu, deleteTable
} from '../controllers/chatBot.controller';

const router = Router();

// TODO: Routes to use
router.get('/', homePage);

// ESTE PRIMER WEBHOOK ES PARA VALIDAR LOS SERVIDORES
router.get('/webhook', tokenVerify);

// ESTE SEGUNDO WEBHOOK ES PARA VALIDAR LOS EVENTOS
// https://youtu.be/KVgsU-fR7wU?list=PLpOqH6AE0tNgqiWwTKtZMUau4PD6XW5Om&t=173
router.post('/webhook', validateEvent);

/**
 *
 * route to send custom message
 *
 * */
router.post('/sendMessage', sendMessageCustom);

/**
 * CRUD TO KEYWORD AND MESSAGE
 */
 // route to save keyword and reply message
router.post('/keyword', saveKeywordMessage);

 // route to list keyword and reply message
router.get('/keyword', listKeywordMessage);

 // route to update keyword and reply message
router.put('/keyword/:id', updateKeywordMessage);

 // route to update keyword and reply message
router.delete('/keyword/:id', deleteKeywordMessage);


/**
 * CRUD TO TEMPLATE IMAGE
 */
// route to save keyword and reply message
router.post('/keyImage', saveTImageMessage);

// route to list keyword and reply message
router.get('/keyImage', listTImageMessage);

// route to update keyword and reply message
router.put('/keyImage/:id', updateTImageMessage);

// route to update keyword and reply message
router.delete('/keyImage/:id', deleteTImageMessage);


/**
 * CRUD TO TEMPLATE VIDEO
 */
// route to save keyword and reply message
router.post('/keyVideo', saveTVideoMessage);

// route to list keyword and reply message
router.get('/keyVideo', listTVideoMessage);

// route to update keyword and reply message
router.put('/keyVideo/:id', updateTVideoMessage);

// route to update keyword and reply message
router.delete('/keyVideo/:id', deleteTVideoMessage);


/**
 * CRUD TO TEMPLATE BUTTON
 */
// route to save keyword and reply message
router.post('/keyButton', saveTButtonMessage);

// route to list keyword and reply message
router.get('/keyButton', listTButtonMessage);

// route to update keyword and reply message
router.put('/keyButton/:id', updateTButtonMessage);

// route to update keyword and reply message
router.delete('/keyButton/:id', deleteTButtonMessage);


/**
 * CRUD TO TEMPLATE CAROUSEL
 */
// route to save keyword and reply message
router.post('/keyCarousel', saveTCarouselMessage);

// route to list keyword and reply message
router.get('/keyCarousel', listTCarouselMessage);

// route to update keyword and reply message
router.put('/keyCarousel/:id', updateTCarouselMessage);

// route to update keyword and reply message
router.delete('/keyCarousel/:id', deleteTCarouselMessage);


/**
 * CRUD TO TEMPLATE CONEXION
 */
// route to save keyword and reply message
router.post('/keyConexion', saveTConexionMessage);

// route to list keyword and reply message
router.get('/keyConexion', listTConexionMessage);

// route to update keyword and reply message
router.put('/keyConexion/:id', updateTConexionMessage);

// route to update keyword and reply message
router.delete('/keyConexion/:id', deleteTConexionMessage);


/**
 * READ AND UPDATE BTN MENU
 */
// route to list keyword and reply message
router.get('/btn_menu', listBTNMenu);

// route to update keyword and reply message
router.put('/btn_menu', updateBTNMenu);

/**
 * TOTAL CLIENTS REGISTER
 * */
router.get('/clients', totalClients);

/**
 * DELETE REGISTER USERS
 * */
router.get('/delete_user', deleteTable);

// TODO: EXPORT
export default router;
