import {pool} from "../../database";
import request from "request";
import config from "../../config";

export function receiveMessage(event){
    let senderID = event.sender.id;
    let messageText = event.message.text;
    let recipientID = event.recipient.id;

    saveIdFB(event);


    evaluateMessage(senderID, messageText);

}

export function receivePayload(event){
    let senderID = event.sender.id;
    let messageText = event.postback.payload;
    let recipientID = event.recipient.id;

    saveIdFB(event);


    evaluateMessage(senderID, messageText);

}

// FUNCION PARA GUARDAR EN LA BD LOS ID's
async function saveIdFB(event) {
    const dataInsert = {
        senderId: event.sender.id,
        recipientId: event.recipient.id,
        timestamp: transformTime(event.timestamp)
    }

    try {
        await pool.query('INSERT INTO userschat SET ?', [dataInsert]);
    } catch (e) {
        console.error(e);
    }
}
// TRANSFORM UNIX TIMESTAMP TO TIME
function transformTime(unixTimestamp) {
    const date = new Date(unixTimestamp);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDay();
    const hour = date.getHours();
    const minut = date.getMinutes();
    const second = date.getSeconds();
    return year + '-' + month + '-' + day + ' ' + hour + ':' + minut + ':' + second;
}

//FUNCIÓN PARA RESPONDER LOS MENSAJES
async function evaluateMessage(recipientId, message){
    console.log('MSG de FB: ', message);
    var finalMessage = 'Para mayor información use los botones del menú ✌';
    let status = false;

    // Search message text to DB
    if (!status){
        try {
            let keys = await pool.query('SELECT * FROM keyword_message');

            keys.forEach( key => {
                if (isContain(message, key.keyword_1) || isContain(message, key.keyword_2) || isContain(message, key.keyword_3)){
                    status = true;
                    finalMessage = key.message;
                }
            });

            if (status){
                sendMessageText(recipientId, finalMessage);
            }
        } catch (e) {
            console.error(e);
        }
    }

    // Search template image to DB
    let objImage = {};
    if (!status){
        try {
            let keys = await pool.query('SELECT * FROM keyword_image');

            keys.forEach( key => {
                if (isContain(message, key.keyword_1) || isContain(message, key.keyword_2) || isContain(message, key.keyword_3)){
                    status = true;
                    objImage = key;
                }
            });

            if (status){
                sendTemplateImagen(recipientId, objImage);
            }
        } catch (e) {
            console.error(e);
        }
    }

    // Search template video to DB
    let objVideo = {};
    if (!status){
        try {
            let keys = await pool.query('SELECT * FROM keyword_video');

            keys.forEach( key => {
                if (isContain(message, key.keyword_1) || isContain(message, key.keyword_2) || isContain(message, key.keyword_3)){
                    status = true;
                    objVideo = key;
                }
            });

            if (status){
                sendTemplateVideo(recipientId, objVideo);
            }
        } catch (e) {
            console.error(e);
        }
    }

    // Search template button to DB
    let objButton = {};
    if (!status){
        try {
            let keys = await pool.query('SELECT * FROM keyword_button');

            keys.forEach( key => {
                if (isContain(message, key.keyword_1) || isContain(message, key.keyword_2)){
                    status = true;
                    objButton = key;
                }
            });

            if (status){
                sendTemplateButton(recipientId, objButton);
            }
        } catch (e) {
            console.error(e);
        }
    }

     // Search template Conexion de button to DB
     let objConexion = {};
     if (!status){
         try {
             let keys = await pool.query('SELECT * FROM keyword_conexion');
 
             keys.forEach( key => {
                 // key general
                 if (isContain(message, key.keyWord)){
                     status = true;
                     objConexion = key;
                 }
                 // keyword_1
                 if (isContain(message, key.keyword_1)){
                     status = true;
                     objConexion = {};
                     finalMessage = key.message1;
                 }
                 // keyword_1
                 if (isContain(message, key.keyword_2)){
                     status = true;
                     objConexion = {};
                     finalMessage = key.message2;
                 }
             });
 
             if (status && objConexion.text !== undefined){
                 sendTemplateConexion(recipientId, objConexion);
             } else if (status && objConexion.text === undefined){
                 sendMessageText(recipientId, finalMessage);
             }
         } catch (e) {
             console.error(e);
         }
     }


      // Search template carousel to DB
      let objCarousel = {};
      if (!status){
          try {
              let keys = await pool.query('SELECT * FROM keyword_carousel');
  
              keys.forEach( key => {
                  if (isContain(message, key.keyword_1) || isContain(message, key.keyword_2)){
                      status = true;
                      objCarousel = key;
                  }
              });
  
              if (status){
                  sendTemplateCarousel(recipientId, objCarousel);
              }
          } catch (e) {
              console.error(e);
          }
      }

    // Search keyword to menu button
    if (!status){
        try {
            let keys = await pool.query('SELECT * FROM menu');

            keys.forEach( key => {
                if (isContain(message, key.keyword)){
                    status = true;
                    finalMessage = key.message;
                }
            });

            if (status){
                sendMessageText(recipientId, finalMessage);
            }
        } catch (e) {
            console.error(e);
        }
    }

    // Message default
    if (!status) {
        sendMessageText(recipientId, finalMessage);
    }

}

//FUNCIÓN PARA CREARME UNA ESTRUCTURA, PARA FACEBOOK ES IMPORTANTE QUE LOS MENSAJES QUE VA A RECIBIR EL SERVIDOR DE FACEBOOK TENGA LA SIGUIENTE ESTRUCTURA
export function sendMessageText(senderId, message){
    console.log('*****  function sendMessageText()')
    var messageData = {
        recipient: {
            id: senderId
        },
        message: {
            text: message
        }
    };
    callSendAPI(messageData);
}

function callSendAPI(messageData){
    request({
        uri:'https://graph.facebook.com/v11.0/me/messages',
        qs: {access_token: config.appTokenFB},
        method: 'POST',
        json: messageData
    }, function(error, response, data){
        if(error){
            console.log('No es posible enviar el mensaje');
        }else{
            console.log("El mensaje fue enviado");
        }
    });

}



//FUNCIÓN PARA REGRESAR TRUE O FALSE SI LA PALABRA(WORD) SE ENCUENTRA DENTRO DE LA SENTENCIA
function isContain(sentence, word){
    return sentence.indexOf(word) > -1;
}

//FUNCIÓN PARA BUSCAR FRACE EN LA BD Y RETORNAR UNA IMAGEN
async function searchToBD(frace) {
    // return sentence.indexOf(word) > -1;
    let words;
    try {

        words = await pool.query('SELECT * FROM imagenes');
    } catch (e) {
        console.error(e);
    }
    let image = '';
    words.forEach((word) => {
        if (frace.indexOf(word.palabra) > -1){
            console.log('SE ENCONTRO');
            image = word.imagen;
        }
    })
    return image;
}

//CREAMOS UNA FUNCIÓN PARA ENVIAR IMÁGENES
export function sendMessageImage(senderId, imagen){
    console.log('BODY-IMG: ', imagen);
    var messageData = {
        recipient: {
            id: senderId
        },
        message: {
            attachment: {
                type: 'image',
                payload: {
                    url: imagen
                }
            }
        }
    };
    callSendAPI(messageData);
}

//FUNCIÓN PARA EL TEMPLATE
function sendMessageTemplate(recipientId){
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'generic',
                    elements: [elemenTemplate()]
                }
            }
        }
    };
    callSendAPI(messageData);
}

function elemenTemplate() {
    return {
        title: "Call Me",
        subtitle: "Blondie",
        item_url: "https://open.spotify.com/track/7HKxTNVlkHsfMLhigmhC0I?si=dc05b52d010a4b18",
        image_url: "https://rebasando.com/images/biblicas/el-nino-y-el-perrito.jpg",
        buttons: [buttonTemplate()]
    }
}

function buttonTemplate() {
    return{
        type: 'web_url',
        url: 'https://open.spotify.com/track/7HKxTNVlkHsfMLhigmhC0I?si=dc05b52d010a4b18',
        title: 'Click Aqui'
    }
}

//PLANTILLA DE LISTA
function listTemplate(recipientId) {

    const requestData = {
        "recipient":{
            "id": recipientId
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "list",
                    "top_element_style": "compact",
                    "elements": [
                        {
                            "title": "Classic T-Shirt Collection",
                            "subtitle": "See all our colors",
                            "image_url": "https://storage.googleapis.com/mmc-elcaribe-bucket/uploads/2021/06/213965b5-dogs_1280p_0-60ba19d1d2c8c.jpg",
                            "buttons": [
                                {
                                    "title": "View",
                                    "type": "web_url",
                                    "url": "https://peterssendreceiveapp.ngrok.io/collection",
                                }
                            ]
                        },
                        {
                            "title": "Classic White T-Shirt",
                            "subtitle": "See all our colors",
                            "image_url": "https://laafiladanavajadeockham.files.wordpress.com/2011/04/paaa-el-zetap.jpg?w=584",
                            "default_action": {
                                "type": "web_url",
                                "url": "https://peterssendreceiveapp.ngrok.io/view?item=100",
                            },
                            "buttons": [
                                {
                                    "title": "View",
                                    "type": "web_url",
                                    "url": "https://peterssendreceiveapp.ngrok.io/collection",
                                }
                            ]
                        },
                        {
                            "title": "Classic Blue T-Shirt",
                            "image_url": "https://assets.simpleviewinc.com/simpleview/image/upload/crm/kansascityks/dog-67_F52CB646-5056-B3A8-49FD530178C90F8D_f5623467-5056-b3a8-496eb2dcf7a4ce5c.jpg",
                            "subtitle": "100% Cotton, 200% Comfortable",
                            "default_action": {
                                "type": "web_url",
                                "url": "https://peterssendreceiveapp.ngrok.io/view?item=101",
                            },
                            "buttons": [
                                {
                                    "title": "Shop Now",
                                    "type": "web_url",
                                    "url": "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                                }
                            ]
                        }
                    ]
                }
            }
        }
    }

    request({
        uri:'https://graph.facebook.com/v11.0/me/messages',
        qs: {access_token: config.appTokenFB},
        method: 'POST',
        json: requestData
    }, function(error, response, data){
        if(error){
            console.log('No es posible enviar el mensaje-NO');
        }else{
            console.log("El mensaje fue enviado-SI: ");
        }
    });
}

//PLANTILLA DE VIDEOS
function mediaTemplate(recipientId) {

    const requestData = {
        "recipient":{
            "id": recipientId
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "media",
                    "elements": [
                        {
                            "media_type": "video",
                            "url": "https://www.facebook.com/PRONABEC/videos/997535544373237",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "https://www.facebook.com/PRONABEC/videos/997535544373237",
                                    "title": "Ver Website",
                                }
                            ]
                        }

                    ]
                }
            }
        }
    }

    request({
        uri:'https://graph.facebook.com/v11.0/me/messages',
        qs: {access_token: config.appTokenFB},
        method: 'POST',
        json: requestData
    }, function(error, response, data){
        if(error){
            console.log('No es posible enviar el mensaje-NO');
        }else{
            console.log("El mensaje fue enviado-SI: ");
        }
    });
}

//PLANTILLA DE LISTA DE BOTONES

function listaBotonTemplate(recipientId) {

    const requestData = {
        "recipient":{
            "id": recipientId
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type":"button",
                    "text":"Qué quieres hacer después?",
                    "buttons":[
                        {
                            "type":"web_url",
                            "url":"http://aulavirtual.sistemas.unmsm.edu.pe/pregrado2017/",
                            "title":"Hola ayuda"
                        },
                        {
                            "type":"web_url",
                            "url":"https://www.unmsm.edu.pe/",
                            "title":"como estas"
                        },
                        {
                            "type":"web_url",
                            "url":"https://www.tripadvisor.com.pe/  Restaurant_Review-g294316-d7139099-Reviews-El_Rancho_de_Robertin-Lima_Lima_Region.html",
                            "title":"cuando la hacemos"
                        }
                    ]
                }
            }
        }
    }

    request({
        uri:'https://graph.facebook.com/v11.0/me/messages',
        qs: {access_token: config.appTokenFB},
        method: 'POST',
        json: requestData
    }, function(error, response, data){
        if(error){
            console.log('No es posible enviar el mensaje-NO');
        }else{
            console.log("El mensaje fue enviado-SI: ");
        }
    });
}

//PLANTILLA DE IMAGEN CON BOTON
function sendTemplateImagen(recipientId, objImage){
    console.log('OBJ-IMAGE-F', objImage);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'generic',
                    elements: [
                      {
                        title: objImage.title,
                        subtitle: objImage.sub_title,
                        image_url: objImage.url_image,
                        buttons: [
                          {
                            title: objImage.name_button,
                            type: "web_url",
                            url: objImage.url_button
                          }
                        ]
                      }
                    ]
                }
            }
       }
   };
   callSendAPI(messageData);
}  

//PLANTILLA DE VIDEO CON BOTON
function sendTemplateVideo(recipientId, objVideo){
    console.log('OBJ-VIDEO-F', objVideo);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'media',
                    elements: [
                      { media_type: "video",
                        url: objVideo.url_video,
                        buttons: [
                          {
                            title: objVideo.name_button,
                            type: "web_url",
                            url: objVideo.url_button
                          }
                        ]
                      }
                    ]
                }
            }
       }
   };
   callSendAPI(messageData);
}  

//PLANTILLA DE LISTA DE BOTONES
function sendTemplateButton(recipientId, objButton){
    console.log('OBJ-BUTTON-F', objButton);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'button',
                    text: objButton.text,
                    buttons: [
                        {
                            title: objButton.name_button1,
                            type: "web_url",
                            url: objButton.url_button1
                        },
                        {
                            title: objButton.name_button2,
                            type: "web_url",
                            url: objButton.url_button2
                        },
                        {
                            title: objButton.name_button3,
                            type: "web_url",
                            url: objButton.url_button3
                        }
                    ]
                }
            }
       }
   };
   callSendAPI(messageData);
}  


//PLANTILLA DE CAROUSEL
function sendTemplateCarousel(recipientId, objCarousel){
    console.log('OBJ-CAROUSEL-F', objCarousel);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'generic',
                    elements: [
                        {
                            title: objCarousel.title1,
                            image_url: objCarousel.url_imagen1
                        },
                        {
                            title: objCarousel.title2,
                            image_url: objCarousel.url_imagen2
                        },
                        {
                            title: objCarousel.title3,
                            image_url: objCarousel.url_imagen3
                        }
                    ]
                }
            }
       }
   };
   callSendAPI(messageData);
}


//PLANTILLA DE CONEXION CON BOTONES
function sendTemplateConexion(recipientId, objConexion){
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'button',
                    text: objConexion.text,
                    buttons: [
                        {
                            title: objConexion.title1,
                            type: "postback",
                            payload: objConexion.keyword_1
                        },
                        {
                            title: objConexion.title2,
                            type: "postback",
                            payload: objConexion.keyword_2
                        },
                        {
                            title: objConexion.title3,
                            type: "postback",
                            payload: objConexion.keyword_3
                        }
                    ]
                }
            }
       }
   };
   callSendAPI(messageData);
}  


export function deployMenuChat (bodyMenu) {
    let request_body = {
        "get_started":{
            "payload":"GET_STARTED"
        },
        "persistent_menu": [
            {
                "locale": "default",
                "composer_input_disabled": false,
                "call_to_actions": [
                    {
                        "type": "postback",
                        "title": bodyMenu[1].title,
                        "payload": "CARE_HELP"
                    },
                    {
                        "type": "postback",
                        "title": bodyMenu[2].title,
                        "payload": "HOW_TO_BUY"
                    },
                    {
                        "type": "web_url",
                        "title": bodyMenu[3].title,
                        "url": bodyMenu[3].message,
                        "webview_height_ratio": "full"
                    }
                ]
            }
        ]
    };
    return new Promise((resolve, reject) => {
        try {
            request({
                uri:'https://graph.facebook.com/v11.0/me/messenger_profile',
                qs: {access_token: config.appTokenFB},
                method: 'POST',
                json: request_body
            }, (error, response, data) => {
                console.log('-------------------------------------------------------------');
                console.log('Logs setup persistent menu & get started button: ', response);
                console.log('-------------------------------------------------------------');
                if(!error){
                    console.log('Setup done');
                }else{
                    // res.send('Something wrongs with setup, please check logs ....');
                    console.log("No se envio el mensaje: ", error);
                }
            });
        }catch (e) {
            reject(e);
        }
    });
}

