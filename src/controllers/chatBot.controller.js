import { pool, queris } from '../database';
import config from '../config';
import request from "request";
import {
    deployMenuChat,
    receiveMessage,
    receivePayload,
    sendMessageImage,
    sendMessageText
} from "./functions/chatBot.functions";

export const homePage = (req, res) => {
    res.json({
        'message': 'Welcome to my application'
    });
}

export const tokenVerify = (req, res) => {
    if(req.query['hub.verify_token'] === config.tokenVerify){
        res.send(req.query['hub.challenge']);

    }else{
        res.send('Tu no tienes que entrar aqui');
    }
}

export const validateEvent = (req, res) => {
    let data = req.body;
    console.log(data);
    if(data.object === 'page'){
        data.entry.forEach(function(pageEntry){

            pageEntry.messaging.forEach(function(messagingEvent){

                if(messagingEvent.message){
                    console.log('INPUT-MESSAGE --> ', messagingEvent);
                    receiveMessage(messagingEvent);
                }else if (messagingEvent.postback){
                    console.log('INPUT-PAYLOAD --> ', messagingEvent);
                    receivePayload(messagingEvent);
                }

            });
        });
        res.sendStatus(200);
    }
}


/*export const menuChat = async (req, res) => {
    let request_body = {
        "get_started":{
            "payload":"GET_STARTED"
        },
        "persistent_menu": [
            {
                "locale": "default",
                "composer_input_disabled": false,
                "call_to_actions": [
                    {
                        "type": "postback",
                        "title": "🔒 ¿Somos Confiables?",
                        "payload": "CARE_HELP"
                    },
                    {
                        "type": "postback",
                        "title": "💳 ¿Cómo Comprar?",
                        "payload": "HOW_TO_BUY"
                    },
                    {
                        "type": "web_url",
                        "title": "🎮 Ver Catálogo de Juegos",
                        "url": "https://www.chiclestore.com/",
                        "webview_height_ratio": "full"
                    }
                ]
            }
        ]
    };
    return new Promise((resolve, reject) => {
        try {
            request({
                uri:'https://graph.facebook.com/v11.0/me/messenger_profile',
                qs: {access_token: config.appTokenFB},
                method: 'POST',
                json: request_body
            }, (error, response, data) => {
                console.log('-------------------------------------------------------------');
                console.log('Logs setup persistent menu & get started button: ', response);
                console.log('-------------------------------------------------------------');
                if(!error){
                    res.send('Setup done');
                }else{
                    res.send('Something wrongs with setup, please check logs ....');
                    console.log("No se envio el mensaje: ", error);
                }
            });
        }catch (e) {
            reject(e);
        }
    });
}*/



//INSTRUCCION PARA ENVIAR MENSAJES A ID DE FACEBOOK MESSENGER
export const sendMessageCustom = async (req, res) => {
    const {message, url_image} = req.body;

    if (message == null || url_image == null) {
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields'
        });

    }

    try {
        let senderIds;
        senderIds = await pool.query('SELECT senderId FROM userschat');
        console.log('**** recipientsIds-> ', senderIds);
        senderIds.forEach((senderId) => {
            if (message !== ''){
                console.log('**** MSG-SEND-TEXT-> ', senderId);
                sendMessageText(senderId.senderId, message);
            }
            if (url_image !== ''){
                console.log('**** MSG-SEND-IMG-> ', senderId);
                sendMessageImage(senderId.senderId, url_image)
            }
        });
    } catch (e) {
        console.error(e);
    }

    res.json(req.body);
}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveKeywordMessage = async (req, res) => {
    const {keyword_1, keyword_2, keyword_3, message} = req.body;

    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || message == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields'
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        message: message
    }

    try {
        await pool.query('INSERT INTO keyword_message SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listKeywordMessage = async (req, res) => {

    try {

        let words = await pool.query('SELECT * FROM keyword_message');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateKeywordMessage = async (req, res) => {
    const {id} = req.params;
    const {keyword_1, keyword_2, keyword_3, message} = req.body;

    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || message == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            body: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        message: message
    }

    try {
        let words = await pool.query('UPDATE keyword_message SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const deleteKeywordMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_message WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

/**
 * CRUD - TEMPLATE IMAGE
 * */
// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveTImageMessage = async (req, res) => {
    const {keyword_1, keyword_2, keyword_3, title, sub_title, url_image, name_button, url_button} = req.body;
    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || title == null || sub_title == null || url_image == null || name_button == null || url_button == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        title: title,
        sub_title: sub_title,
        url_image: url_image,
        name_button: name_button,
        url_button: url_button
    }

    try {
        await pool.query('INSERT INTO keyword_image SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listTImageMessage = async (req, res) => {

    try {
        // words = await pool.query('INSERT INTO imagenes set ?', [data]);
        let words = await pool.query('SELECT * FROM keyword_image');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateTImageMessage = async (req, res) => {
    const {id} = req.params;
    const {keyword_1, keyword_2, keyword_3, title, sub_title, url_image, name_button, url_button} = req.body;

    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || title == null || sub_title == null || url_image == null || name_button == null || url_button == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        title: title,
        sub_title: sub_title,
        url_image: url_image,
        name_button: name_button,
        url_button: url_button
    }

    try {
        let words = await pool.query('UPDATE keyword_image SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const deleteTImageMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_image WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}



/**
 * CRUD - TEMPLATE VIDEO
 * */
// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveTVideoMessage = async (req, res) => {
    const {keyword_1, keyword_2, keyword_3, url_video, name_button, url_button} = req.body;
    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || url_video == null || name_button == null || url_button == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        url_video: url_video,
        name_button: name_button,
        url_button: url_button
    }

    try {
        await pool.query('INSERT INTO keyword_video SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listTVideoMessage = async (req, res) => {

    try {
        // words = await pool.query('INSERT INTO imagenes set ?', [data]);
        let words = await pool.query('SELECT * FROM keyword_video');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateTVideoMessage = async (req, res) => {
    const {id} = req.params;
    const {keyword_1, keyword_2, keyword_3, url_video, name_button, url_button} = req.body;

    if (keyword_1 == null || keyword_2 == null || keyword_3 == null || url_video == null || name_button == null || url_button == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        keyword_3: keyword_3,
        url_video: url_video,
        name_button: name_button,
        url_button: url_button
    }

    try {
        let words = await pool.query('UPDATE keyword_video SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const deleteTVideoMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_video WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}




/**
 * CRUD - TEMPLATE BUTTON
 * */
// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveTButtonMessage = async (req, res) => {
    const {keyword_1, keyword_2, text, name_button1, url_button1, name_button2, url_button2, name_button3, url_button3} = req.body;
    if (keyword_1 == null || keyword_2 == null || text == null || name_button1 == null || url_button1 == null || name_button2 == null || url_button2 == null || name_button3 == null || url_button3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        text: text,
        name_button1: name_button1,
        url_button1: url_button1,
        name_button2: name_button2,
        url_button2: url_button2,
        name_button3: name_button3,
        url_button3: url_button3
    }

    try {
        await pool.query('INSERT INTO keyword_button SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listTButtonMessage = async (req, res) => {

    try {
        // words = await pool.query('INSERT INTO imagenes set ?', [data]);
        let words = await pool.query('SELECT * FROM keyword_button');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateTButtonMessage = async (req, res) => {
    const {id} = req.params;
    const {keyword_1, keyword_2, text, name_button1, url_button1, name_button2, url_button2, name_button3, url_button3} = req.body;

    if (keyword_1 == null || keyword_2 == null || text == null || name_button1 == null || url_button1 == null || name_button2 == null || url_button2 == null || name_button3 == null || url_button3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        text: text,
        name_button1: name_button1,
        url_button1: url_button1,
        name_button2: name_button2,
        url_button2: url_button2,
        name_button3: name_button3,
        url_button3: url_button3
    }

    try {
        let words = await pool.query('UPDATE keyword_button SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const deleteTButtonMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_button WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}



/**
 * CRUD - TEMPLATE CONEXION
 * */
// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveTConexionMessage = async (req, res) => {
    const {keyWord, text, title1, keyword_1, message1, title2, keyword_2, message2, title3, keyword_3} = req.body;
    if (keyWord == null || text == null || title1 == null || keyword_1 == null || message1 == null || title2 == null || keyword_2 == null || message2 == null || title3 == null || keyword_3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyWord: keyWord,
        text: text,
        title1: title1,
        keyword_1: keyword_1,
        message1: message1,
        title2: title2,
        keyword_2: keyword_2,
        message2: message2,
        title3: title3,
        keyword_3: keyword_3
    }

    try {
        await pool.query('INSERT INTO keyword_conexion SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listTConexionMessage = async (req, res) => {

    try {
        let words = await pool.query('SELECT * FROM keyword_conexion');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateTConexionMessage = async (req, res) => {
    const {id} = req.params;
    const {keyWord, text, title1, keyword_1, message1, title2, keyword_2, message2, title3, keyword_3} = req.body;

    if (keyWord == null || text == null || title1 == null || keyword_1 == null || message1 == null || title2 == null || keyword_2 == null || message2 == null || title3 == null || keyword_3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyWord: keyWord,
        text: text,
        title1: title1,
        keyword_1: keyword_1,
        message1: message1,
        title2: title2,
        keyword_2: keyword_2,
        message2: message2,
        title3: title3,
        keyword_3: keyword_3
    }

    try {
        let words = await pool.query('UPDATE keyword_conexion SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const deleteTConexionMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_conexion WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}





/**
 * CRUD - TEMPLATE CAROUSEL
 * */
// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const saveTCarouselMessage = async (req, res) => {
    const {keyword_1, keyword_2, title1, url_imagen1, title2, url_imagen2, title3, url_imagen3} = req.body;
    if (keyword_1 == null || keyword_2 == null || title1 == null || url_imagen1 == null || title2 == null || url_imagen2 == null || title3 == null || url_imagen3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        title1: title1,
        url_imagen1: url_imagen1,
        title2: title2,
        url_imagen2: url_imagen2,
        title3: title3,
        url_imagen3: url_imagen3
    }

    try {
        await pool.query('INSERT INTO keyword_carousel SET ?', [dataInsert]);
        return res.status(200).json({
            msg: 'keyword and message save successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// GUARDAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const listTCarouselMessage = async (req, res) => {

    try {
        // words = await pool.query('INSERT INTO imagenes set ?', [data]);
        let words = await pool.query('SELECT * FROM keyword_carousel');
        return res.status(200).json({
            data: words
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR PALABRA CLAVE Y MENSAJE DE RESPUESTA
export const updateTCarouselMessage = async (req, res) => {
    const {id} = req.params;
    const {keyword_1, keyword_2, title1, url_imagen1, title2, url_imagen2, title3, url_imagen3} = req.body;

    if (keyword_1 == null || keyword_2 == null || title1 == null || url_imagen1 == null || title2 == null || url_imagen2 == null || title3 == null || url_imagen3 == null){
        return res.status(400).json({
            msg: 'Bad Request. Please fill all fields',
            req: req.body
        });

    }

    const dataInsert = {
        keyword_1: keyword_1,
        keyword_2: keyword_2,
        title1: title1,
        url_imagen1: url_imagen1,
        title2: title2,
        url_imagen2: url_imagen2,
        title3: title3,
        url_imagen3: url_imagen3
    }

    try {
        let words = await pool.query('UPDATE keyword_carousel SET ? WHERE id = ?', [dataInsert, id]);
        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// ELIMINAR CAROUSEL MESSAGE
export const deleteTCarouselMessage = async (req, res) => {
    const {id} = req.params;

    try {
        let words = await pool.query('DELETE FROM keyword_carousel WHERE id = ?', [id]);
        return res.status(200).json({
            msg: 'Delete successfull'
        });
    } catch (e) {
        console.error(e);
    }

}

// LISTAR BTN MENU
export const listBTNMenu = async (req, res) => {

    try {

        let resMenu = await pool.query('SELECT * FROM menu');
        return res.status(200).json({
            data: resMenu
        });
    } catch (e) {
        console.error(e);
    }

}

// EDITAR BTN MENU
export const updateBTNMenu = async (req, res) => {
    const body = req.body;

    try {
        await pool.query('UPDATE menu SET ? WHERE id = ?', [{message: body[0].message}, body[0].id]);
        await pool.query('UPDATE menu SET ? WHERE id = ?', [{title: body[1].title, message: body[1].message}, body[1].id]);
        await pool.query('UPDATE menu SET ? WHERE id = ?', [{title: body[2].title, message: body[2].message}, body[2].id]);
        await pool.query('UPDATE menu SET ? WHERE id = ?', [{title: body[3].title, message: body[3].message}, body[3].id]);

        deployMenuChat(body);

        return res.status(200).json({
            msg: 'Update successfull'
        });
    } catch (e) {
        console.error(e);
    }

}


// RETURN TOTAL CLIENTS
export const totalClients = async (req, res) => {

    try {
        let clients = await pool.query('SELECT COUNT(*) AS total_clients FROM userschat');
        return res.status(200).json({
            msg: 'Request success',
            total_clients: clients[0].total_clients
        });
    } catch (e) {
        console.error(e);
    }

}

// DELETE REGISTER
export const deleteTable = async (req, res) => {
    try {
        let resQuery = await pool.query('DELETE FROM userschat');
        return res.status(200).json({
            msg: 'Request success',
            response: resQuery
        });
    } catch (e) {
        console.error(e);
    }
}
