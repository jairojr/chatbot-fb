import express from 'express';
import cors from 'cors';
import config from './config';
import chatBotRoutes from './routes/chatBot.routes';
import authroute from './routes/auth.route';

const app = express();

// import temporal - dependecia obsoleta
var request = require('request');

// TODO: SETINGS
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use(cors());

// TODO: ROUTES
app.use(chatBotRoutes);
app.use(authroute);

// TODO: EXPORT
export default app;




