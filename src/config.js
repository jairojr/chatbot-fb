import {config} from "dotenv";
config();

export default {
    port: process.env.PORT,
    appTokenFB: process.env.APP_TOKEN_FB,
    tokenVerify: process.env.TOKEN_VERIFY,
    apimap: process.env.URL_API_MAP,
    dbUser: process.env.DB_USER || '',
    dbPassword: process.env.DB_PASSWORD || '',
    dbServer: process.env.DB_SERVER || '',
    dbDatabase: process.env.DB_DATABASE || ''
}
