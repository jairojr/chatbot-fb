# chatbot-fb

### Archivo .env 📋

_Composicion de las variables de entorno del proyecto_

```
PORT = < puerto desde donde se ejecuta la aplicacion >
APP_TOKEN_FB = < token de la aplicacion de FB >
TOKEN_VERIFY = < token de verificacion de la aplicacion de FB >
```

## Comenzando 🚀

_Antes de iniciar la aplicacion instala las dependencias_

```
npm install
```

_Para iniciar la aplicacion en modo DEV ejecuta lo siguiente_

```
npm run dev
```
