
window.onload = chargeListCard();

function chargeListCard (){
    let divContainer = document.getElementById('content_card');
    let cardList = '';
    fetch('http://localhost:4000/keyButton')
        .then(response => response.json())
        .then(keywords => {
            keywords.data.forEach(keyword => {
                var templateCard = getTemplateCardButton(keyword.id, keyword.keyword_1, keyword.keyword_2, keyword.text, keyword.name_button1, keyword.url_button1, keyword.name_button2, keyword.url_button2, keyword.name_button3, keyword.url_button3);
                cardList = cardList + templateCard;
            })
            divContainer.innerHTML = cardList;
        });
};

function createNewCard() {
    let formData = {
        keyword_1: document.getElementById('input_key_1').value,
        keyword_2: document.getElementById('input_key_2').value,
        text: document.getElementById('input_key_3').value,
        name_button1: document.getElementById('input_key_4').value,
        url_button1: document.getElementById('input_key_5').value,
        name_button2: document.getElementById('input_key_6').value,
        url_button2: document.getElementById('input_key_7').value,
        name_button3: document.getElementById('input_key_8').value,
        url_button3: document.getElementById('input_key_9').value
    }

    console.log('DATA-CREATE:', formData);

    fetch('http://localhost:4000/keyButton/', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function editCard(id) {
    console.log('EDITAR:', id);

    let formData = {
        keyword_1: document.getElementById('input_1_' + id).value,
        keyword_2: document.getElementById('input_2_' + id).value,
        text: document.getElementById('input_3_' + id).value,
        name_button1: document.getElementById('input_4_' + id).value,
        url_button1: document.getElementById('input_5_' + id).value,
        name_button2: document.getElementById('input_6_' + id).value,
        url_button2: document.getElementById('input_7_' + id).value,
        name_button3: document.getElementById('input_8_' + id).value,
        url_button3: document.getElementById('input_9_' + id).value
    }

    console.log('DATA-EDIT:', formData);

    fetch('http://localhost:4000/keyButton/' + id, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function deleteCard(id) {
    console.log('ELIMINAR:', id);
    fetch('http://localhost:4000/keyButton/' + id, {
        method: 'DELETE'
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-DELETE:', res);
            chargeListCard();
        });
}

function changeBTN(id) {
    document.getElementById('btn_edit_' + id).setAttribute('style','display: none');
    document.getElementById('btn_save_' + id).setAttribute('style','');
}
