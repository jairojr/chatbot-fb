window.onload = chargeCardMenu();

function chargeCardMenu (){
    let divContainer = document.getElementById('content_card');
    let cardMenu = '';
    fetch('http://localhost:4000/btn_menu')
        .then(response => response.json())
        .then(res => {
            cardMenu = getTemplateCardMenu(res);
            /*keywords.data.forEach(keyword => {
                var templateCard = getTemplateCardVideo(keyword.id, keyword.keyword_1, keyword.keyword_2, keyword.keyword_3, keyword.url_video, keyword.name_button, keyword.url_button);
                cardList = cardList + templateCard;
            })*/
            divContainer.innerHTML = cardMenu;
        });
}

function editCardMenu(idVal1, idVal2, idVal3, idVal4) {
    console.log('ID-S:', idVal1, idVal2, idVal3, idVal4);
    let formData = [
        {
            id: idVal1,
            message: document.getElementById('input_1_' + idVal1).value
        },
        {
            id: idVal2,
            title: document.getElementById('input_2_' + idVal2).value,
            message: document.getElementById('input_3_' + idVal2).value
        },
        {
            id: idVal3,
            title: document.getElementById('input_4_' + idVal3).value,
            message: document.getElementById('input_5_' + idVal3).value
        },
        {
            id: idVal4,
            title: document.getElementById('input_6_' + idVal4).value,
            message: document.getElementById('input_7_' + idVal4).value
        }
    ];

    console.log('DATA-EDIT:', formData);

    fetch('http://localhost:4000/btn_menu', {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT-MENU:', res);
            chargeCardMenu();
        });
}

function changeBTN(id) {
    document.getElementById('btn_edit_' + id).setAttribute('style','display: none');
    document.getElementById('btn_save_' + id).setAttribute('style','');
}
