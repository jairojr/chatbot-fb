
window.onload = chargeListCard();

function chargeListCard (){
    let divContainer = document.getElementById('content_card');
    let cardList = '';
    fetch('http://localhost:4000/keyword')
        .then(response => response.json())
        .then(keywords => {
            keywords.data.forEach(keyword => {
                var templateCard = getTemplateCard(keyword.id, keyword.keyword_1, keyword.keyword_2, keyword.keyword_3, keyword.message);
                cardList = cardList + templateCard;
            })
            divContainer.innerHTML = cardList;
        });



}

function createNewCard() {
     let formData = {
        keyword_1: document.getElementById('input_key_1').value,
        keyword_2: document.getElementById('input_key_2').value,
        keyword_3: document.getElementById('input_key_3').value,
        message: document.getElementById('input_message').value
    }

    console.log('DATA-CREATE:', formData);

    fetch('http://localhost:4000/keyword/', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function editCard(id) {
    console.log('EDITAR:', id);

    let formData = {
        keyword_1: document.getElementById('input_1_' + id).value,
        keyword_2: document.getElementById('input_2_' + id).value,
        keyword_3: document.getElementById('input_3_' + id).value,
        message: document.getElementById('input_4_' + id).value
    }

    console.log('DATA-EDIT:', formData);

    fetch('http://localhost:4000/keyword/' + id, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function deleteCard(id) {
    console.log('ELIMINAR:', id);
    fetch('http://localhost:4000/keyword/' + id, {
        method: 'DELETE'
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-DELETE:', res);
            chargeListCard();
        });
}

function changeBTN(id) {
    document.getElementById('btn_edit_' + id).setAttribute('style','display: none');
    document.getElementById('btn_save_' + id).setAttribute('style','');
}
