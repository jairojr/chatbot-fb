
window.onload = chargeListCard();

function chargeListCard (){
    let divContainer = document.getElementById('content_card');
    let cardList = '';
    fetch('http://localhost:4000/keyConexion')
        .then(response => response.json())
        .then(dataCards => {
            dataCards.data.forEach(dataCard => {
                var templateCard = getTemplateCardConexion(dataCard);
                cardList = cardList + templateCard;
            })
            divContainer.innerHTML = cardList;
        });
};

function createNewCard() {
    let formData = {
        keyWord: document.getElementById('input_key_1').value,
        text: document.getElementById('input_key_2').value,
        title1: document.getElementById('input_key_3').value,
        keyword_1: document.getElementById('input_key_4').value,
        message1: document.getElementById('input_key_5').value,
        title2: document.getElementById('input_key_6').value,
        keyword_2: document.getElementById('input_key_7').value,
        message2: document.getElementById('input_key_8').value,
        title3: document.getElementById('input_key_9').value,
        keyword_3: document.getElementById('input_key_10').value
    }

    console.log('DATA-CREATE:', formData);

    fetch('http://localhost:4000/keyConexion/', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function editCard(id) {
    console.log('EDITAR:', id);

    let formData = {
        keyWord: document.getElementById('input_1_' + id).value,
        text: document.getElementById('input_2_' + id).value,
        title1: document.getElementById('input_3_' + id).value,
        keyword_1: document.getElementById('input_4_' + id).value,
        message1: document.getElementById('input_5_' + id).value,
        title2: document.getElementById('input_6_' + id).value,
        keyword_2: document.getElementById('input_7_' + id).value,
        message2: document.getElementById('input_8_' + id).value,
        title3: document.getElementById('input_9_' + id).value,
        keyword_3: document.getElementById('input_10_' + id).value
    }

    fetch('http://localhost:4000/keyConexion/' + id, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-EDIT:', res);
            chargeListCard();
        });
}

function deleteCard(id) {
    console.log('ELIMINAR:', id);
    fetch('http://localhost:4000/keyConexion/' + id, {
        method: 'DELETE'
    })
        .then(response => response.json())
        .then(res => {
            console.log('RESPONSE-DELETE:', res);
            chargeListCard();
        });
}

function changeBTN(id) {
    document.getElementById('btn_edit_' + id).setAttribute('style','display: none');
    document.getElementById('btn_save_' + id).setAttribute('style','');
}
