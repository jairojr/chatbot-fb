window.onload = chargeTotalClients();

function chargeTotalClients (){
    let inputClients = document.getElementById('input_clients');

    fetch('http://localhost:4000/clients')
        .then(response => response.json())
        .then(res => {
            inputClients.value = res.total_clients;
        });
}

function sendMessage() {
    let formData = {
        message: document.getElementById('input_message').value,
        url_image: document.getElementById('input_imagen').value
    }
    fetch('http://localhost:4000/sendMessage', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.json())
        .then(res => {
            console.log('RES-SERVER: ', res);
        });
}

function eliminarRegistros() {
    fetch('http://localhost:4000/delete_user')
        .then(response => response.json())
        .then(res => {
            console.log('RES-SERVER: ', res);
        });
}