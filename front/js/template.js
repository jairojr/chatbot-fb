function getTemplateCard(id, keyword_1, keyword_2, keyword_3, message) {
    var templateCard = `<section class="caja">
                <div class="cambio5">
                  <div class="card mb-3 cambio6">
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_1_${id}">Palabra clave N°1</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_1_${id}" value="${keyword_1}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_2_${id}">Palabra clave N°2</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_2_${id}" value="${keyword_2}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_3_${id}">Palabra clave N°3</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_3_${id}" value="${keyword_3}">
                    </div>
                    <div class="card-body bg-transparent border-primary">
                      <label for="input_4_${id}"><p class="card-text" align="justify">Mensaje de respuesta:</p></label><br><br>
                      <input type="text" name="nombre" class="nombre1" id="input_4_${id}" value="${message}">
                    </div>
                    <div class="card-footer  border-primary cambio4">
                      <button class="bton" onclick="changeBTN(${id})" id="btn_edit_${id}" style="">Editar</button>
                      <button class="bton" onclick="editCard(${id})" id="btn_save_${id}" style="display: none">Guardar</button>
                      <button class="bton" onclick="deleteCard(${id})">Eliminar</button>
                    </div>
                  </div>
                </div>
              </section>`;
    return templateCard;
}

function getTemplateCardImage(id, keyword_1, keyword_2, keyword_3, title, sub_title, url_image, name_button, url_button) {
    var templateCard = `<section class="caja">
                <div class="cambio5">
                  <div class="card mb-3 cambio6">
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_1_${id}">Palabra clave N°1</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_1_${id}" value="${keyword_1}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_2_${id}">Palabra clave N°2</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_2_${id}" value="${keyword_2}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                      <label for="input_3_${id}">Palabra clave N°3</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_3_${id}" value="${keyword_3}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                        <label for="input_4_${id}">Titulo</label><br>
                        <input type="text" name="nombre" class="nombre" id="input_4_${id}" value="${title}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                        <label for="input_5_${id}">Subtitulo</label><br>
                        <input type="text" name="nombre" class="nombre" id="input_5_${id}" value="${sub_title}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                        <label for="input_6_${id}">URL de la Imagen</label><br>
                        <input type="text" name="nombre" class="nombre" id="input_6_${id}" value="${url_image}">
                    </div>
                    <div class="card-header bg-transparent border-primary align-self-center">
                        <label for="input_7_${id}">Nombre del boton</label><br>
                        <input type="text" name="nombre" class="nombre" id="input_7_${id}" value="${name_button}">
                    </div>
                    <div class="card-body bg-transparent border-primary align-self-center">
                        <label for="input_8_${id}">URL del boton</label><br>
                        <input type="text" name="nombre" class="nombre" id="input_8_${id}" value="${url_button}">
                    </div>
                    <div class="card-footer  border-primary cambio4">
                      <button class="bton" onclick="changeBTN(${id})" id="btn_edit_${id}" style="">Editar</button>
                      <button class="bton" onclick="editCard(${id})" id="btn_save_${id}" style="display: none">Guardar</button>
                      <button class="bton" onclick="deleteCard(${id})">Eliminar</button>
                    </div>
                  </div>
                </div>
              </section>`;
    return templateCard;
}


function getTemplateCardVideo(id, keyword_1, keyword_2, keyword_3, url_video, name_button, url_button) {
  var templateCard = `<section class="caja">
  <div class="cambio5">
    <div class="card mb-3 cambio6">
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_1_${id}">Palabra clave N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_1_${id}" value="${keyword_1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_2_${id}">Palabra clave N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_2_${id}" value="${keyword_2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_3_${id}">Palabra clave N°3</label><br>
        <input type="text" name="nombre" class="nombre" id="input_3_${id}" value="${keyword_3}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_4_${id}">URL del Video</label><br>
          <input type="text" name="nombre" class="nombre" id="input_4_${id}" value="${url_video}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_5_${id}">Nombre del boton</label><br>
          <input type="text" name="nombre" class="nombre" id="input_5_${id}" value="${name_button}">
      </div>
      <div class="card-body bg-transparent border-primary align-self-center">
          <label for="input_6_${id}">URL del boton</label><br>
          <input type="text" name="nombre" class="nombre" id="input_6_${id}" value="${url_button}">
      </div>
      <div class="card-footer  border-primary cambio4">
        <button class="bton" onclick="changeBTN(${id})" id="btn_edit_${id}" style="">Editar</button>
        <button class="bton" onclick="editCard(${id})" id="btn_save_${id}" style="display: none">Guardar</button>
        <button class="bton" onclick="deleteCard(${id})">Eliminar</button>
      </div>
    </div>
  </div>
</section>`;
  return templateCard;
}

function getTemplateCardButton(id, keyword_1, keyword_2, text, name_button1, url_button1, name_button2, url_button2, name_button3, url_button3) {
  var templateCard = `<section class="caja">
  <div class="cambio5">
    <div class="card mb-3 cambio6">
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_1_${id}">Palabra clave N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_1_${id}" value="${keyword_1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_2_${id}">Palabra clave N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_2_${id}" value="${keyword_2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_3_${id}">Texto</label><br>
        <input type="text" name="nombre" class="nombre" id="input_3_${id}" value="${text}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_4_${id}">Nombre del boton N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_4_${id}" value="${name_button1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_5_${id}">URL del boton N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_5_${id}" value="${url_button1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_6_${id}">Nombre del boton N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_6_${id}" value="${name_button2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_7_${id}">URL del boton N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_7_${id}" value="${url_button2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_8_${id}">Nombre del boton N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_8_${id}" value="${name_button3}">
      </div>
      <div class="card-body bg-transparent border-primary align-self-center">
          <label for="input_9_${id}">URL del boton N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_9_${id}" value="${url_button3}">
      </div>
      <div class="card-footer  border-primary cambio4">
        <button class="bton" onclick="changeBTN(${id})" id="btn_edit_${id}" style="">Editar</button>
        <button class="bton" onclick="editCard(${id})" id="btn_save_${id}" style="display: none">Guardar</button>
        <button class="bton" onclick="deleteCard(${id})">Eliminar</button>
      </div>
    </div>
  </div>
</section>`;
  return templateCard;
}


function getTemplateCardCarousel(id, keyword_1, keyword_2, title1, url_imagen1, title2, url_imagen2, title3, url_imagen3) {
  var templateCard = `<section class="caja">
  <div class="cambio5">
    <div class="card mb-3 cambio6">
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_1_${id}">Palabra clave N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_1_${id}" value="${keyword_1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_2_${id}">Palabra clave N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_2_${id}" value="${keyword_2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_3_${id}">Titulo de la imagen N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_3_${id}" value="${title1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_4_${id}">URL de la imagen N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_4_${id}" value="${url_imagen1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_5_${id}">Titulo de la imagen N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_5_${id}" value="${title2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_6_${id}">URL de la imagen N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_6_${id}" value="${url_imagen2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_7_${id}">Titulo de la imagen N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_7_${id}" value="${title3}">
      </div>
      <div class="card-body bg-transparent border-primary align-self-center">
          <label for="input_8_${id}">URL de la imagen N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_8_${id}" value="${url_imagen3}">
      </div>
      <div class="card-footer  border-primary cambio4">
        <button class="bton" onclick="changeBTN(${id})" id="btn_edit_${id}" style="">Editar</button>
        <button class="bton" onclick="editCard(${id})" id="btn_save_${id}" style="display: none">Guardar</button>
        <button class="bton" onclick="deleteCard(${id})">Eliminar</button>
      </div>
    </div>
  </div>
</section>`;
  return templateCard;
}

function getTemplateCardMenu(objMenu) {
    let data = objMenu.data;
    return `<section class="caja">
              <div class="cambio5">
                <div class="card mb-3 cambio6">
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_1_${data[0].id}">Mensaje Inicial</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_1_${data[0].id}" value="${data[0].message}">
                  </div>
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_2_${data[1].id}">Titulo del Menu N°1</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_2_${data[1].id}" value="${data[1].title}">
                  </div>
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_3_${data[1].id}">Mensaje del Menu N°1</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_3_${data[1].id}" value="${data[1].message}">
                  </div>
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_4_${data[2].id}">Titulo del Menu N°2</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_4_${data[2].id}" value="${data[2].title}">
                  </div>
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_5_${data[2].id}">Mensaje del Menu N°2</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_5_${data[2].id}" value="${data[2].message}">
                  </div>
                  <div class="card-header bg-transparent border-primary align-self-center">
                    <label for="input_6_${data[3].id}">Titulo del Menu N°3</label><br>
                    <input type="text" name="nombre" class="nombre" id="input_6_${data[3].id}" value="${data[3].title}">
                  </div>
                  <div class="card-body bg-transparent border-primary align-self-center">
                      <label for="input_7_${data[3].id}">URL del Menu N°3</label><br>
                      <input type="text" name="nombre" class="nombre" id="input_7_${data[3].id}" value="${data[3].message}">
                  </div>
                  <div class="card-footer  border-primary cambio4">
                    <button class="bton" onclick="changeBTN(${data[0].id})" id="btn_edit_${data[0].id}" style="">Editar</button>
                    <button class="bton" onclick="editCardMenu(${data[0].id}, ${data[1].id}, ${data[2].id}, ${data[3].id})" id="btn_save_${data[0].id}" style="display: none">Guardar</button>
                  </div>
                </div>
              </div>
            </section>`;
}


function getTemplateCardConexion(dataCard) {
  var templateCard = `<section class="caja">
  <div class="cambio5">
    <div class="card mb-3 cambio6">
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_1_${dataCard.id}">Palabra clave principal</label><br>
        <input type="text" name="nombre" class="nombre" id="input_1_${dataCard.id}" value="${dataCard.keyWord}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_2_${dataCard.id}">Texto</label><br>
        <input type="text" name="nombre" class="nombre" id="input_2_${dataCard.id}" value="${dataCard.text}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_3_${dataCard.id}">Nombre del boton N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_3_${dataCard.id}" value="${dataCard.title1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_4_${dataCard.id}">Palabra clave N°1</label><br>
        <input type="text" name="nombre" class="nombre" id="input_4_${dataCard.id}" value="${dataCard.keyword_1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_5_${dataCard.id}">Mensaje de respuesta N°1:</label><br>
        <input type="text" name="nombre" class="nombre" id="input_5_${dataCard.id}" value="${dataCard.message1}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_6_${dataCard.id}">Nombre del boton N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_6_${dataCard.id}" value="${dataCard.title2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
        <label for="input_7_${dataCard.id}">Palabra clave N°2</label><br>
        <input type="text" name="nombre" class="nombre" id="input_7_${dataCard.id}" value="${dataCard.keyword_2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_8_${dataCard.id}">Mensaje de respuesta N°2:</label><br>
          <input type="text" name="nombre" class="nombre" id="input_8_${dataCard.id}" value="${dataCard.message2}">
      </div>
      <div class="card-header bg-transparent border-primary align-self-center">
          <label for="input_9_${dataCard.id}">Nombre del boton N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_9_${dataCard.id}" value="${dataCard.title3}">
      </div>
      <div class="card-body bg-transparent border-primary align-self-center">
          <label for="input_10_${dataCard.id}">Palabra clave N°3</label><br>
          <input type="text" name="nombre" class="nombre" id="input_10_${dataCard.id}" value="${dataCard.keyword_3}">
      </div>
      <div class="card-footer  border-primary cambio4">
        <button class="bton" onclick="changeBTN(${dataCard.id})" id="btn_edit_${dataCard.id}" style="">Editar</button>
        <button class="bton" onclick="editCard(${dataCard.id})" id="btn_save_${dataCard.id}" style="display: none">Guardar</button>
        <button class="bton" onclick="deleteCard(${dataCard.id})">Eliminar</button>
      </div>
    </div>
  </div>
</section>`;
  return templateCard;
}
